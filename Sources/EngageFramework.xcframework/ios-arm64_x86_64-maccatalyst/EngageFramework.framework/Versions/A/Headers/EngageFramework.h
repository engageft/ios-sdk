//
//  EngageFramework.h
//  EngageFramework
//
//  Created by Elijah Rappaport on 4/7/22.
//

#import <Foundation/Foundation.h>

//! Project version number for EngageFramework.
FOUNDATION_EXPORT double EngageFrameworkVersionNumber;

//! Project version string for EngageFramework.
FOUNDATION_EXPORT const unsigned char EngageFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <EngageFramework/PublicHeader.h>


