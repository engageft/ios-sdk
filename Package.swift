// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "EngageSDK",
    platforms: [
        .macOS(.v10_15), .iOS(.v14), .tvOS(.v14)
    ],
    products: [
        .library(
            name: "EngageSDK",
            targets: ["EngageSDKTargets"]),
    ],
    dependencies: [
        .package(url: "https://github.com/freshOS/Networking", from: "1.0.0")
    ],
    targets: [
        .binaryTarget(name: "EngageFramework",
                      path: "./Sources/EngageFramework.xcframework"
        ),
        .target(name: "EngageSDKTargets",
                dependencies: [
                    .target(name: "EngageFramework"),
                    "Networking"
                ]
        )
    ]
)
